# Deploy Prometheus & Grafana with Docker - Version 1.0

It's a simple tool to deploy easly a Prometheus and a plugged Grafana in containers.

## Requirements
* Docker Engine
* Docker Compose

## Usage
This prometheus.yml contains just a basic configuration. You must customize it with your own rules and hosts to srape.

When your Prometheus configuration is ready, just launch containers with docker-compose : ``` docker-compose up -d ```

* To access to your Prometheus : localhost:9090
* To access to your Grafana : localhost:3000. Login/password : admin/admin

## Credits
Tool developped by Jonas Chopin-Revel on Licence GNU-AGPLv3.

Gitlab : https://framagit.org/jcr/prometheus-and-grafana-with-docker

**Share and improve it. It's free !**
